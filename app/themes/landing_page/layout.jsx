import React from 'react';

export default class Layout extends React.Component {

  render() {
    return(
      <html xmlns="http://www.w3.org/1999/xhtml" lang="en">
        <head>
          <title>{this.props.site.name}</title>

          <link rel="icon" type="image/png" href={this.props.assetPath('favicon.png')} sizes="32x32" />

          <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css' />

          <link rel="stylesheet" href={this.props.assetPath('/themes/landing_page/stylesheets/font-awesome.min.css')} />
          <link rel="stylesheet" href={this.props.assetPath('/themes/landing_page/frontend.css')} />
          <script type="text/javascript" src={this.props.assetPath('/themes/landing_page/frontend.js')} />

          <link href={this.props.assetPath('/themes/landing_page/stylesheets/bootstrap.min.css')} rel="stylesheet" />
          <link href={this.props.assetPath('/themes/landing_page/stylesheets/style.css')} rel="stylesheet" />
          <link href={this.props.assetPath('/themes/landing_page/stylesheets/mobile.css')} rel="stylesheet" media="(max-width:1170px)" />

          <script dangerouslySetInnerHTML={{__html: `
            require('themes/landing_page/frontend/javascripts/main');
          `}} />
        </head>
        <body>
          {this.props.children}

          <script src={this.props.assetPath('/themes/landing_page/javascripts/jquery.min.js')}></script>
          <script src={this.props.assetPath('/themes/landing_page/javascripts/bootstrap.min.js')}></script>
          <script src={this.props.assetPath('/themes/landing_page/javascripts/script.js')}></script>
        </body>
      </html>
    )
  }

}
