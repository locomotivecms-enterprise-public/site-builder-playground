import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';

export default class Features extends React.Component {

  render() {
    var content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="section" id={this.props.block.id} className="sec1_1" block={this.props.block} background={content.background}>
        <SiteBuilderList className="container" list={content.features} separatorEvery={3} separatorClassName="clr">
          {(feature, key, index) => (
            <div key={key} className={`col-md-4 bullit1 ${index > content.features.length - 4 ? 'bullit1_no_border' : ''} ${index % 3 === 1 ? 'bullit1_border_sides' : ''}`}>
              <span className={`bullit1_icon fa ${feature.icon}`}/>
              <h3>{feature.title}</h3>
              <SiteBuilderText tag="div" className="bullit1_text" value={feature.description} />
            </div>
          )}
        </SiteBuilderList>
      </SiteBuilderBlock>
    );
  }

}
