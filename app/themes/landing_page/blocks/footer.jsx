import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class Footer extends React.Component {

  renderLinksCol(col) {
    var content = this.props.block.content;
    var title = content[`titleCol${col}`];
    var list  = content[`linksCol${col}`];

    if (title === null || title === '') return null;

    return (
      <div className="col">
        <SiteBuilderText className="text" value={title} />
        <SiteBuilderList list={list}>
          {(link, key) => (
            <SiteBuilderLink key={key} value={link} allowEmpty={false} />
          )}
        </SiteBuilderList>
      </div>
    )
  }

  render() {
    var content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="footer" className="footer1" block={this.props.block}>
        <div className="container">
          <div className="line"></div>

          <div className="footer1_info">
            <SiteBuilderText tag="h3" value={content.name} />
            <SiteBuilderText className="text" value={content.description} />

            <div className="footer1_info_socials">
              <SiteBuilderLink value={{ url: content.twitter, target: true }} allowEmpty={false}>
                <i className="fa fa-twitter"/>
              </SiteBuilderLink>
              <SiteBuilderLink value={{ url: content.google, target: true }} allowEmpty={false}>
                <i className="fa fa-google-plus"/>
              </SiteBuilderLink>
              <SiteBuilderLink value={{ url: content.facebook, target: true }} allowEmpty={false}>
                <i className="fa fa-facebook"/>
              </SiteBuilderLink>
              <SiteBuilderLink value={{ url: content.linkedin, target: true }} allowEmpty={false}>
                <i className="fa fa-linkedin"/>
              </SiteBuilderLink>
              <SiteBuilderLink value={{ url: content.github, target: true }} allowEmpty={false}>
                <i className="fa fa-github"/>
              </SiteBuilderLink>
              <SiteBuilderLink value={{ url: content.pinterest, target: true }} allowEmpty={false}>
                <i className="fa fa-pinterest"/>
              </SiteBuilderLink>
              <SiteBuilderLink value={{ url: content.instagram, target: true }} allowEmpty={false}>
                <i className="fa fa-instagram"/>
              </SiteBuilderLink>
            </div>
          </div>

          <div className="footer1_links">
            {this.renderLinksCol(1)}
            {this.renderLinksCol(2)}
            {this.renderLinksCol(3)}
          </div>

          <div className="clr"></div>

        </div>
      </SiteBuilderBlock>
    );
  }

}
