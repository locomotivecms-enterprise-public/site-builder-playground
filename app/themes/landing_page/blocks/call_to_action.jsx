import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class CallToAction extends React.Component {

  render() {
    var content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="section" id={this.props.block.id} className="sec9_1" block={this.props.block}>
        <div className="container">
          <SiteBuilderText tag="h2" className="small" value={content.text} />
          <div className="sec9_1_buy">
            <SiteBuilderLink className="sec9_1_buy_btn" value={content.action} />
            <SiteBuilderLink className="sec9_1_buy_read" value={content.link} />
          </div>
        </div>
      </SiteBuilderBlock>
    );
  }

}
