import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';

export default class Companies extends React.Component {

  render() {
    var content = this.props.block.content;
    return(
      <SiteBuilderBlock tag="section" id={this.props.block.id} className="sec6_1" block={this.props.block}>
        <div className="container">
          <SiteBuilderText tag="h2" className="small" value={content.title} />
          <SiteBuilderList className="sec6_1_logos" list={content.list}>
            {(company, key) => (
              <img key={key} src={this.props.assetPath(company.image)} alt={company.name} />
            )}
          </SiteBuilderList>
        </div>
      </SiteBuilderBlock>
    );
  }

}
