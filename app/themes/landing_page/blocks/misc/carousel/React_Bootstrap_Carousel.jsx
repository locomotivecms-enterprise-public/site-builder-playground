/* React_Boostrap_Carousel.jsx*/
import React from 'react';
import classNames from '../classnames';
import ReactCarouselIndicators from './React_Carousel_Indicators';
import ReactCarouselControls from './React_Carousel_Controls';
import ReactCarouselItem from './React_Carousel_Item';

export default class React_Bootstrap_Carousel extends React.Component {

  static get defaultProps() {
    return {
      indicators:         true,
      controls:           true,
      slideshowSpeed:     7000,
      defaultActiveIndex: 0,
      wrap:               true,
      children:           [],
      animation:          true,
      className:          "",
      pauseOnHover:       true
    }
  }

  constructor(props, context) {
    super(props, context);

    this.handleIndicatorsClick  = this.handleIndicatorsClick.bind(this);
    this.handleControlsClick    = this.handleControlsClick.bind(this);
    this.handleMouseOver        = this.handleMouseOver.bind(this);
    this.handleMouseOut         = this.handleMouseOut.bind(this);
    this.handlePrev = this.handlePrev.bind(this);
    this.handleNext = this.handleNext.bind(this);
    this.handleItemAnimateOutEnd = this.handleItemAnimateOutEnd.bind(this);

    const { defaultActiveIndex } = props;

    this.state = {
      activeIndex: defaultActiveIndex != null ? defaultActiveIndex : 0,
      previousActiveIndex: null,
      direction: null,
    };

    this.isUnmounted = false;
  }

  componentWillReceiveProps(nextProps) {
    const activeIndex = this.getActiveIndex();

    if (
      nextProps.activeIndex != null &&
      nextProps.activeIndex !== activeIndex
    ) {
      clearTimeout(this.timeout);

      this.setState({
        previousActiveIndex: activeIndex,
        direction: nextProps.direction != null ?
          nextProps.direction :
          this.getDirection(activeIndex, nextProps.activeIndex),
      });
    }
  }

  componentDidUpdate(prevProps, prevState) {
    const activeIndex = this.getActiveIndex();

    if (this.props.activeIndex != null) {
      clearTimeout(this.timeout);
      this.select(this.props.activeIndex);
    }
  }

  componentDidMount() {
    this.waitForNext();
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
    this.isUnmounted = true;
  }

  handleMouseOver() {
    if (this.props.pauseOnHover) {
      this.pause();
    }
  }

  handleMouseOut() {
    if (this.isPaused) {
      this.play();
    }
  }

  handleControlsClick(call) {
    clearTimeout(this.timeout);
    this.isPaused = false;
    if (call == 'prev')
      this.handlePrev(null);
    else
      this.handleNext(null);
  }

  handleIndicatorsClick(index) {
    clearTimeout(this.timeout);
    this.isPaused = false;
    this.select(index);
  }

  handlePrev(e) {
    let index = this.getActiveIndex() - 1;

    if (index < 0) {
      if (!this.props.wrap) return;
      index = this.props.children.length - 1;
    }

    this.select(index, e, 'prev');
  }

  handleNext(e) {
    let index = this.getActiveIndex() + 1;
    const count = this.props.children.length;

    if (index > count - 1) {
      if (!this.props.wrap) return;
      index = 0;
    }

    this.select(index, e, 'next');
  }

  handleItemAnimateOutEnd() {
    this.setState({
      previousActiveIndex: null,
      direction: null
    }, () => {
      this.waitForNext();
    });
  }

  getActiveIndex() {
    const activeIndexProp = this.props.activeIndex;
    return activeIndexProp != null ? activeIndexProp : this.state.activeIndex;
  }

  getDirection(prevIndex, index) {
    if (prevIndex === index) {
      return null;
    }

    return prevIndex > index ? 'prev' : 'next';
  }

  select(index, e, direction) {
    clearTimeout(this.timeout);

    // TODO: Is this necessary? Seems like the only risk is if the component
    // unmounts while handleItemAnimateOutEnd fires.
    if (this.isUnmounted) {
      return;
    }

    const previousActiveIndex = this.getActiveIndex();
    direction = direction || this.getDirection(previousActiveIndex, index);

    const { onSelect } = this.props;

    if (onSelect) {
      if (onSelect.length > 1) {
        // React SyntheticEvents are pooled, so we need to remove this event
        // from the pool to add a custom property. To avoid unnecessarily
        // removing objects from the pool, only do this when the listener
        // actually wants the event.
        if (e) {
          e.persist();
          e.direction = direction;
        } else {
          e = { direction };
        }

        onSelect(index, e);
      } else {
        onSelect(index);
      }
    }

    if (this.props.activeIndex == null && index !== previousActiveIndex) {
      if (this.state.previousActiveIndex != null) {
        // If currently animating don't activate the new index.
        // TODO: look into queueing this canceled call and
        // animating after the current animation has ended.
        return;
      }

      this.setState({
        activeIndex: index,
        previousActiveIndex,
        direction
      });
    }
  }

  waitForNext() {
    const { animation, slideshowSpeed, activeIndex: activeIndexProp } = this.props;

    if (!this.isPaused && animation && slideshowSpeed && activeIndexProp == null) {
      this.timeout = setTimeout(this.handleNext, slideshowSpeed);
    }
  }

  // This might be a public API.
  pause() {
    this.isPaused = true;
    clearTimeout(this.timeout);
  }

  // This might be a public API.
  play() {
    this.isPaused = false;
    this.waitForNext();
  }

  renderItems(items) {
    const { animation } = this.props;
    const { previousActiveIndex, direction } = this.state;

    const activeIndex = this.getActiveIndex();

    return items.map((item, index) => {
      const active = index === activeIndex;
      const previousActive = animation && index === previousActiveIndex;

      return (
        <ReactCarouselItem
          key={index}
          active={active}
          index={index}
          animateOut={previousActive}
          animateIn={animation && active && previousActiveIndex != null}
          direction={direction}
          onAnimateOutEnd={previousActive ? this.handleItemAnimateOutEnd : null}
        >
          {item}
        </ReactCarouselItem>
      )
    });
  }

  render() {
    const { children, indicators, controls, slideshowSpeed, leftImage, rightImage } = this.props;
    const { previousActiveIndex, direction } = this.state;
    const activeIndex = this.getActiveIndex();
    const classes = classNames("carousel slide", this.props.className);

    return (
      <div
        className={classes}
        onMouseOver={this.handleMouseOver}
        onMouseOut={this.handleMouseOut}
      >
        {this.props.indicators && <ReactCarouselIndicators data={children} activeIndex={activeIndex} indClick={this.handleIndicatorsClick}/>}
        <div className="carousel-inner" role="listbox">
          {this.renderItems(children)}
        </div>
        {this.props.controls && <ReactCarouselControls dataLength={children.length} leftImage={leftImage} rightImage={rightImage} prev={this.handlePrev} next={this.handleNext} controlsClick={this.handleControlsClick}/>}
      </div>

    );
  }

}
