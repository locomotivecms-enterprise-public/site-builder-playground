/*React_Carousel_Item.jsx*/
import React from 'react';
import ReactDOM from 'react-dom';
import classNames from '../classnames';

import TransitionEvents from './TransitionEvents';

export default class React_Carousel_Item extends React.Component {

  static get propTypes() {
    return {
      direction: React.PropTypes.oneOf(['prev', 'next']),
      onAnimateOutEnd: React.PropTypes.func,
      active: React.PropTypes.bool,
      animateIn: React.PropTypes.bool,
      animateOut: React.PropTypes.bool,
      index: React.PropTypes.number
    }
  }

  static get defaultProps() {
    return {
      active:     false,
      animateIn:  false,
      animateOut: false
    }
  }

  constructor(props, context) {
    super(props, context);

    this.handleAnimateOutEnd = this.handleAnimateOutEnd.bind(this);

    this.state = {
      direction: null,
    };

    this.isUnmounted = false;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.active !== nextProps.active) {
      this.setState({ direction: null });
    }
  }

  componentDidUpdate(prevProps) {
    const { active } = this.props;
    const prevActive = prevProps.active;

    if (!active && prevActive) {
      TransitionEvents.addEndEventListener(
        ReactDOM.findDOMNode(this), this.handleAnimateOutEnd
      );
    }

    if (active !== prevActive) {
      setTimeout(() => this.startAnimation(), 20);
    }
  }

  componentWillUnmount() {
    this.isUnmounted = true;
  }

  handleAnimateOutEnd() {
    if (this.isUnmounted) {
      return;
    }

    if (this.props.onAnimateOutEnd) {
      this.props.onAnimateOutEnd(this.props.index);
    }
  }

  startAnimation() {
    if (this.isUnmounted) {
      return;
    }

    this.setState({
      direction: this.props.direction === 'prev' ? 'right' : 'left',
    });
  }

  render() {
    const props = Object.assign({}, this.props);
    const direction   = this.props.direction;
    const active      = this.props.active;
    const animateIn   = this.props.animateIn;
    const animateOut  = this.props.animateOut;
    const className   = this.props.className;
    delete props.direction;
    delete props.active;
    delete props.animateIn;
    delete props.animateOut;
    delete props.className;
    delete props.onAnimateOutEnd;
    delete props.index;

    const classes = {
      item: true,
      active: active && !animateIn || animateOut,
    };

    if (direction && active && animateIn) {
      classes[direction] = true;
    }
    if (this.state.direction && (animateIn || animateOut)) {
      classes[this.state.direction] = true;
    }

    return (
      <div
        {...props}
        className={classNames(className, classes)}>
        {this.props.children}
      </div>
    );
  }
}
