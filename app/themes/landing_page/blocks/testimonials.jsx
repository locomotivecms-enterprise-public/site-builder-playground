import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';

export default class Testimonials extends React.Component {

  constructor() {
    super();
    this.state = { current: 0 }
  }

  componentDidMount() {
    this.setHeight();
  }

  componentDidUpdate() {
    this.setHeight();
  }

  nextSlide() {
    let content = this.props.block.content;
    let next = this.state.current + 1;

    if (next > content.people.length - 1) next = 0;

    this.setState({ current: next });
  }

  previousSlide() {
    let content = this.props.block.content;
    let previous = this.state.current - 1;

    if (previous < 0) previous = content.people.length - 1;

    this.setState({ current: previous });
  }

  selectSlide(index) {
    this.setState({ current: index });
  }

  setHeight() {
    let _this = this;

    $(document).ready(function() {
      let slides = $(_this.refs.slider).find('li');
      var maxHeight = Math.max.apply(null, slides.map(function () {
        return $(this).height();
      }).get());

      if (maxHeight > 0) {
        $(_this.refs.slider).find('ul').height(maxHeight);
      }
    })
  }

  render() {
    var content = this.props.block.content;
    return(
      <SiteBuilderBlock tag="section" id={this.props.block.id} className="sec5_1" block={this.props.block}>
        <div className="container">
          <SiteBuilderText tag="h2" value={content.title} />

          <div className="row sec5_1_control">
            <div className="col-xs-1">
              <i onClick={this.previousSlide.bind(this)} className="arrow fa fa-angle-left" />
            </div>
            <div className="col-xs-10">
              <SiteBuilderList className="sec5_1_pager" list={content.people}>
                {(person, key, index) => (
                  <a onClick={this.selectSlide.bind(this, index)} key={key} className={index === this.state.current ? 'active' : ''}>
                    <img src={this.props.assetPath(person.picture)} alt="" />
                  </a>
                )}
              </SiteBuilderList>
            </div>
            <div className="col-xs-1">
              <i onClick={this.nextSlide.bind(this)} className="arrow fa fa-angle-right" />
            </div>
          </div>

          <div className="row ">
            <div className="col-xs-12 sec5_1_slider" ref="slider">
              <SiteBuilderList tag="ul" list={content.people}>
                {(person, key, index) => (
                  <li key={key} className={index === this.state.current ? 'active' : ''}>
                    <SiteBuilderText className="sec5_1_text" value={person.testimonial} />
                    <SiteBuilderText className="sec5_1_author" value={person.name} />
                  </li>
                )}
              </SiteBuilderList>
            </div>
          </div>
        </div>
      </SiteBuilderBlock>
    );
  }

}
