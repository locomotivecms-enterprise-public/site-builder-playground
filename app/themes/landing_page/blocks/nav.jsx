import React from 'react';
import ReactDOM from 'react-dom';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';

export default class Nav extends React.Component {

  componentDidMount() {
    var $nav = $(ReactDOM.findDOMNode(this.nav));
    this.$mobileMenu = $('<a class="mobile_menu" data-open="nav1" id="mobile_menu"></a>').insertBefore($nav);
  }

  componentWillUnmount() {
    this.$mobileMenu.remove();
  }

  getLogoStyle() {
    let logoUrl = this.props.assetPath(this.props.block.content.logo);

    if (typeof(logoUrl) === 'undefined' || logoUrl === null)
      return {};
    else
      return { backgroundImage: `url(${logoUrl})` };
  }

  render() {
    var content = this.props.block.content;

    return (
      <SiteBuilderBlock id="nav" ref={(el) => this.nav = el} tag="nav" className="nav1" block={this.props.block}>
        <a className="mobile_menu_close"></a>
        <div className="container">
          <div className="flex">
            <a href="/" className="logo1" style={this.getLogoStyle()}></a>
            <SiteBuilderList list={content.links} className="nav1_links">
              {(link, key, index) => (
                <li key={key} className={index == 0 ? 'active' : ''}>
                  <a href={this.props.urlTo(link.url_type, link.url)}>{link.label}</a>
                </li>
              )}
            </SiteBuilderList>
            <SiteBuilderList list={content.auth_links} className="nav1_auth">
              {(link, key) => (
                <SiteBuilderText tag="a" key={key} href={link.url} className={link.highlighted ? 'nav1_btn1 blue' : 'nav1_btn1'} value={link.label} />
              )}
            </SiteBuilderList>
          </div>
        </div>
      </SiteBuilderBlock>
    )
  }

}
