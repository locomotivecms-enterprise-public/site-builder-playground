import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

import { isBlank } from './misc/utils';
import BootstrapCarousel from './misc/carousel/React_Bootstrap_Carousel';

export default class Carousel extends React.Component {

  componentDidMount() {
    document.addEventListener('editing_list_item', this.onSlideEditing.bind(this), false);
  }

  onSlideEditing(event) {
    var item = event.detail.item;

    if (item.blockId !== this.props.block.id) return null;

    this.carousel.wrappedComponent.select(item.position);
  }

  slideStyle(slide) {
    return {
      backgroundImage: `url("${this.props.assetPath(slide.image)}")`,
    }
  }

  renderSlide(slide, index) {
    return <div key={index} className="item-inner" style={this.slideStyle(slide)}>
      <div className="item-content">
        <SiteBuilderText tag="h2" value={slide.heading} />
      </div>
    </div>
  }

  render() {
    var content = this.props.block.content;
    var pause   = this.props.contentEditing === true;

    return(
      <SiteBuilderBlock tag="section" className="carousel" block={this.props.block}>
        <SiteBuilderList
          tag={BootstrapCarousel}
          list={content.slides}
          slideshowSpeed={pause ? 0 : content.speed}
          controls={content.sideNavigation}
          indicators={content.bottomNavigation}
          ref={(ref) => this.carousel = ref}
        >
          {(element, key) => this.renderSlide(element, key)}
        </SiteBuilderList>
      </SiteBuilderBlock>
    );
  }

}
