import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class Feature extends React.Component {

  render() {
    var content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="section" id={this.props.block.id} className="sec4_1" block={this.props.block}>
        <div className="container">
          <div className="row">
            <div className="col-md-5">
              <SiteBuilderText className="sec3_1_pretitle" value={content.preTitle} />
              <div className="clr"></div>
              <SiteBuilderText tag="h2" value={content.title} />
              <SiteBuilderText className="sec3_1_text" value={content.description} />
              <SiteBuilderLink className="sec3_1_btn" value={content.action} allowEmpty={false}/>
            </div>
            <div className="col-md-7">
              <img src={this.props.assetPath(content.image)} alt="" className="img-responsive"/>
            </div>
          </div>
        </div>
      </SiteBuilderBlock>
    );
  }

}
