import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class Team extends React.Component {

  render() {
    var content = this.props.block.content;


    return(
      <SiteBuilderBlock tag="section" id={this.props.block.id} className="sec7_1" block={this.props.block}>
        <div className="container">
          <SiteBuilderText tag="h2" value={content.title}/>
          <SiteBuilderText className="sec7_1_posttitle" value={content.postTitle}/>
          <SiteBuilderList list={content.people} className="sec7_1_team_holder">
            {(person, key) => (
              <div key={key} className="sec7_1_team">
                <img src={this.props.assetPath(person.picture)} alt={person.name} />
                <SiteBuilderText tag="h3" value={person.name}/>
                <SiteBuilderText className="sec7_1_text" value={person.bio}/>
                <div className="sec7_1_socials">
                  <SiteBuilderLink value={{ url: person.twitter, target: true }} allowEmpty={false}>
                    <i className="fa fa-twitter"/>
                  </SiteBuilderLink>
                  <SiteBuilderLink value={{ url: person.google, target: true }} allowEmpty={false}>
                    <i className="fa fa-google-plus"/>
                  </SiteBuilderLink>
                  <SiteBuilderLink value={{ url: person.facebook, target: true }} allowEmpty={false}>
                    <i className="fa fa-facebook"/>
                  </SiteBuilderLink>
                  <SiteBuilderLink value={{ url: person.linkedin, target: true }} allowEmpty={false}>
                    <i className="fa fa-linkedin"/>
                  </SiteBuilderLink>
                  <SiteBuilderLink value={{ url: person.github, target: true }} allowEmpty={false}>
                    <i className="fa fa-github"/>
                  </SiteBuilderLink>
                  <SiteBuilderLink value={{ url: person.pinterest, target: true }} allowEmpty={false}>
                    <i className="fa fa-pinterest"/>
                  </SiteBuilderLink>
                  <SiteBuilderLink value={{ url: person.instagram, target: true }} allowEmpty={false}>
                    <i className="fa fa-instagram"/>
                  </SiteBuilderLink>
                </div>
              </div>
            )}
          </SiteBuilderList>
        </div>
      </SiteBuilderBlock>
    );
  }

}
