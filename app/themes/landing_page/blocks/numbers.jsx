import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class Numbers extends React.Component {

  render() {
    var content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="section" id={this.props.block.id} className="sec8_1" block={this.props.block}>
        <div className="container">
          <SiteBuilderText tag="h2" className="small white" value={content.title}/>
          <SiteBuilderText className="sec8_1_posttitle" value={content.postTitle}/>
          <SiteBuilderList list={content.elements}>
            {(element, key) => (
              <div key={key} className="sec8_1_block">
                <i className={`icon fa ${element.icon}`} alt="" />
                <div className="big">{element.number}</div>
                <div className="small">{element.label}</div>
              </div>
            )}
          </SiteBuilderList>
        </div>
      </SiteBuilderBlock>
    );
  }

}
