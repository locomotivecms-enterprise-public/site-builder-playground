import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';

export default class Header extends React.Component {

  render() {
    var content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="header" className="header1" block={this.props.block} background={content.background}>
        <div className="container">
          <SiteBuilderText className="header1_pretitle" value={content.preTitle} allowEmpty={false} />
          <SiteBuilderText tag="h1" value={content.title} />
          <SiteBuilderText className="header1_posttitle" value={content.postTitle} allowEmpty={false} />
          <SiteBuilderList list={content.buttons} className="header1_buttons">
            {(button, key) => (
              <SiteBuilderText
                tag="a"
                key={key}
                href={this.props.urlTo(button.url_type, button.url)}
                className={button.highlighted ? 'header1_button1' : 'header1_button2'}
                value={button.label} />
            )}
          </SiteBuilderList>
        </div>
      </SiteBuilderBlock>
    );
  }

}
