import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class Work extends React.Component {

  render() {
    var content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="section" id={this.props.block.id} className="sec2_1" block={this.props.block} background={content.background}>
        <div className="container">
          <SiteBuilderText tag="h2" className="white" value={content.title} />
          <SiteBuilderText className="sec2_1_subtitle" value={content.description} />
          <SiteBuilderList list={content.projects}>
            {(project, key) => (
              <div key={key} className="col-md-4 our_works">
                <div className="our_works_bg">
                  <img src={this.props.assetPath(project.image)} />
                </div>
                <SiteBuilderText tag="h3" value={project.title} />
              </div>
            )}
          </SiteBuilderList>
          <div className="clr"></div>
          <SiteBuilderLink className="header1_button2 our_works_button" value={content.action} />
        </div>
      </SiteBuilderBlock>
    );
  }

}
