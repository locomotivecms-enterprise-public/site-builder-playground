import React from 'react';
import SiteBuilderBlock from './common/block';
import SiteBuilderList from './common/list';
import SiteBuilderText from './common/text';
import SiteBuilderLink from './common/link';

export default class SimpleFooter extends React.Component {

  render() {
    var content = this.props.block.content;

    return(
      <SiteBuilderBlock tag="footer" className="footer1_2" block={this.props.block}>
        <div className="footer1_dark">
          <div className="container">
            <SiteBuilderText className="left" value={content.name}/>
            <SiteBuilderList list={content.links} className="right">
              {(link, key) => (
                <SiteBuilderLink key={key} value={link} allowEmpty={false} />
              )}
            </SiteBuilderList>
          </div>
        </div>
      </SiteBuilderBlock>
    );
  }

}
